app.component('review-list',{
    props:{
        reviews:{
            type: Array,
            required: true
        }
    },
    template:
    /*html*/
    `<div class ="review-container">
    <h3> Reviews: </h3>
    <ul>
    <li v-for="(review, index) in reviews" :key="index">
    <h4> {{review.name}} gave {{review.rating}} stars</h4>
    <p>"{{review.review}}"</p>
    <p>{{review.recommend}}, of course! </p>
   
    </li>
    </ul>
    </div>
    `
})