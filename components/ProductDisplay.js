app.component('product-display',{
    props:{
        premium: {
            type: Boolean,
            required:true
        }
    },
    template:
    /*html*/
    `<div class="product-display">
         <div class="product-container">
           <div class="product-image">
             <img :class="{outOfStockImg : !stock}" :src="image">

           </div>
           <div class="product-info">
               <h1>{{title}}</h1>

               <p v-if= "onSale" > {{sale}}  </p>
               <p> Shipping: {{shipping}} </p>
               <p v-if= "stock" > In Stock  </p>
               <p v-else> Stock Out</p>
               <a :href="url"> For more... </a>
               <h3> Available Colors: </h3>
               <div v-for= "(variant, index) in variants" :key="variant.id" @mouseover="updateVariant(index)" class="color-circle" :style="{'background-color': variant.color} "> <p :style="styles"> {{ variant.color }}</p> </div>
               <h3> Sizes available: </h3>
               <div v-for= "size in sizes" :key="size.id"> {{ size.size }} </div>
               <button class="button" @click="addToCart" :disabled="!stock" :class="{disabledButton: !stock}" > Add to cart </button>
               <button class="button" @click="remove" > Remove </button>
           </div>
         </div>
         <review-list v-if="reviews.length" :reviews="reviews"></review-list>
         <review-form @review-submitted="addReview"></review-form>
       </div>`,
    data(){
        return{
            brand: 'Vue Mastery',
            product: 'Socks',
            selectedVariant: 0,
            url:'https://www.happysocks.com/gl/',
            onSale: true,
            sizes:[{id:11,size:'S'}, {id:12,size:'M'}, {id:13,size:'L'},{id:14,size:'XL'}],
            variants:[
                {id: 111, color: 'Green', image:'./assets/images/socks_green.jpg', quantity:45},
                {id: 112, color: 'Blue', image:'./assets/images/socks_blue.jpg', quantity:0}
            ],
            styles: {
                marginLeft: '60px'
            },
            reviews:[]
        }

    },
    computed:{
        title(){
            return this.brand+' '+ this.product;
        },
        image(){
            return this.variants[this.selectedVariant].image
        },
        stock(){
            return this.variants[this.selectedVariant].quantity
        },
        sale(){
            return this.brand+ ' '+ this.product+' is  on sale!'
        },
        shipping(){
           if (this.premium){
               return 'Free!!!'
           }
           return '2.99$'
        }
    },
    methods:{
        addToCart(){
            this.$emit('add-to-cart', this.variants[this.selectedVariant].id)
        },
        updateVariant(index){
            this.selectedVariant = index

        },
        remove(){
            this.$emit('remove-from-cart', this.variants[this.selectedVariant].id)
        },
        addReview(review){
            this.reviews.push(review)
        }
    }
})